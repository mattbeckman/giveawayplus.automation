#!/bin/bash

wget -O giveawaylisting_source.html giveawaylisting.com
grep -oP 'http://amzn.to/\S+(?="\ target)' giveawaylisting_source.html | head -n 10 > giveaways.txt

wget -O official_source.html https://www.amazon.com/ga/giveaways/
grep -oP 'https://www.amazon.com/ga/p/\S+(?=\?)' official_source.html | head -n 10 >> giveaways.txt

python3 parser.py