#!/usr/bin/env python
import time
import unittest
import requests
#import pickle
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys

class FrontendTests(unittest.TestCase):

    def setUp(self):
        """
        Setup WebDriver
        """
        desired_cap = {
            'platform': "Mac OS X 10.9",
            'browserName': "chrome",
            'version': "56"
        }
        options = webdriver.ChromeOptions()
        options.binary_location = '/usr/bin/google-chrome'
        options.add_argument("user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/56.0.2924.76 Chrome/56.0.2924.76 Safari/537.36")
        service_log_path = "{}/chromedriver.log".format('/home/matt/automation/log')
        service_args = ['--verbose --no-sandbox']

        self.driver = webdriver.Chrome('/usr/bin/chromedriver', 
            chrome_options=options, 
            service_args=service_args, 
            service_log_path=service_log_path)

    def tearDown(self):
        self.driver.close()
        self.driver.quit()

    def DISABLED_test_chrome(self):
        self.driver.get('http://www.google.com')
        print(self.driver.page_source)
        pass

    def test_do_giveaway(self):
        with open('giveaways.txt') as f:
            content = f.readlines()
            content = [x.strip('\n') for x in content]

        wait = WebDriverWait(self.driver, 10)
        for url in content:
            self.driver.get(url)
            time.sleep(6)

            # login
            try:
                email = self.driver.find_element_by_name("email")
                password = self.driver.find_element_by_name("password")
                email.send_keys('xxxxxxxx')
                password.send_keys('xxxxxxx')
                password.send_keys(Keys.RETURN)
                time.sleep(5)
            except:
                pass

            # source code
            try:
                html_source = self.driver.page_source
                
                with open('jquery.min.js', 'r') as jquery_js: 
                    jquery = jquery_js.read()
                    self.driver.execute_script(jquery)

                script = "return jQuery.ajax({ type: 'POST', url: 'https://giveaway.plus/api/post-r1HU2S64up1XFUNkGnrSbr.php', data: { 'gid': location.href.substring(location.href.indexOf('/p/') + 3, location.href.indexOf('/p/') + 19), 'html': document.documentElement.outerHTML }, async: false });"
                self.driver.execute_script(script)
                time.sleep(6)

            except:
                pass

            time.sleep(6)

if __name__ == '__main__':
    unittest.main()